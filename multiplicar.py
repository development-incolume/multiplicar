import sys
import pytest


def multiplicar(*args):
    lista = list()
    produto = 1
    try:
        for i in range(len(args)):
            lista.append(float(args[i]))
            produto *= float(args[i])
        #print(args, lista)
        return produto
        pass
    except ValueError:
        raise
    except:
        print(sys.exc_info())

def test_num():
    assert multiplicar(0, 0) == 0
    assert multiplicar(4, 0) == 0
    assert multiplicar(3, 1) == 3
    assert multiplicar(4, 1) == 4
    assert multiplicar(4.1, 3) == 12.299999999999999
    pass

def test_str():
    assert multiplicar('0', '0') == 0
    assert multiplicar('4', '0') == 0
    assert multiplicar('4.1', '3', '.7') == 8.61
    with pytest.raises(ValueError) as e:
        multiplicar('a', 'b')
    e.match(r"could not convert string to float")
    pass



if __name__ == '__main__':
    print(multiplicar(2,3))
    pass